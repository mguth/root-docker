# FROM sauerburger/pyroot3
FROM sauerburger/pyroot3:bcb2e194ac572b9f1e3d8203863fbe4cb717d4f9


## ensure locale is set during build
ENV LANG C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

USER root

ENV ROOTSYS=/opt/root
ENV PATH=$ROOTSYS/bin:$PATH
ENV LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH   

RUN pip3 install uproot && \
    pip3 install root_numpy && \
    pip3 install root_pandas && \
    pip3 install matplotlib && \
    pip3 install numpy h5py scipy && \
    pip3 install seaborn && \
    pip3 install hep_ml && \
    pip3 install sklearn && \
    pip3 install tensorflow && \
    pip3 install keras && \
    pip3 install tables && \
    pip3 install papermill pydot Pillow && \
    pip3 install jupyter && \
    pip3 install jupyterlab


RUN apt-get update && \
    apt-get install -y git debconf-utils && \
#     echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
#     echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
#     apt-get install -y krb5-user && \
    apt-get install -y nodejs && \
    apt-get install -y vim less screen graphviz python3-tk wget && \
    apt-get install -y jq tree hdf5-tools bash-completion

USER user

ENTRYPOINT ["/usr/local/bin/entry_point.sh"]
CMD ["bash"]
    
